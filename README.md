# Code Challenge: Authorizer

### Development environment

To run the Authorizer app yourself you'll need install the dependencies:
* Java 8 JDK
* sbt

1. Install Java JDK
```bash
brew tap caskroom/versions
brew cask install adoptopenjdk/openjdk/adoptopenjdk8
```

2. Install sbt
```bash
brew install sbt
```

### Running the app

```bash
sbt run
```

Sample input data:
```json
{"account": {"active-card": true, "available-limit": 100}}
{"transaction": {"merchant": "Burger King", "amount": 20, "time":"2019-02-13T10:00:00.000Z"}}
{"transaction": {"merchant": "Habbib's", "amount": 90, "time": "2019-02-13T11:00:00.000Z"}}
```

### Running the app from binaries

```bash
java -jar Authorizer-assembly-1.0.jar
```

Note: Binaries could be created using [sbt-assembly](https://github.com/sbt/sbt-assembly) plugging:
```bash
sbt assembly 
```
by default, it is located in `target/scala-2.12/Authorizer-assembly-1.0.jar`

### Running test

```bash
sbt test
```

### Overview

1. The staring point of this app is `Main` object. It defines the concrete implementations of required interfaces,
and starts the authorizer streaming graph.

2. `AuthorizerGraph` interface, implemented by `DefaultAuthorizerGraph` singleton object, is the main component 
of this solution. It defines the source, flows and sink used to parse, validate and write expected inputs in a stream way 
(`AuthorizerAction`), using [Akka Streams](https://doc.akka.io/docs/akka/current/stream/stream-introduction.html).
The implemented stream approach was built defining the next flow:

```scala
 input ~> readJsonFlow ~> authorizeFlow ~> writeJsonFlow ~> output
```

3. `AuthorizerApi` is used by `AuthorizerFlow` to validate incoming transactions and retrieve expected result. 
It was implemented using a event-driven pattern (`EventDrivenAuthorizerApi`), 
using [Akka Typed Actor](https://doc.akka.io/docs/akka/current/typed/actors.html) (`AuthorizerActor`).

4. Transaction business rules were done by `TransactionValidator` interface, using the [Cake Pattern](https://medium.com/rahasak/scala-cake-pattern-e0cd894dae4e)
to compose and stack required validations (see `DefaultTransactionValidator`).

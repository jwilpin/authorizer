package nubank.authorizer.model

import org.joda.time.DateTime

case class Transaction(merchant: String, amount: Long, time: DateTime)

package nubank.authorizer.model

case class Account(activeCard: Boolean, availableLimit: Long)

package nubank.authorizer.stream

import akka.NotUsed
import akka.stream.ClosedShape
import akka.stream.scaladsl.GraphDSL.Implicits._
import akka.stream.scaladsl.{GraphDSL, RunnableGraph}
import nubank.authorizer.api.AuthorizerApi

trait AuthorizerGraph extends InputSource with OutputSink with JsonReaderFlow with AuthorizerFlow with JsonWriterFlow {

  def graph(implicit authorizerApi: AuthorizerApi): RunnableGraph[NotUsed] = {
    RunnableGraph.fromGraph(GraphDSL.create() { implicit builder: GraphDSL.Builder[NotUsed] =>
      input ~> readJsonFlow ~> authorizeFlow ~> writeJsonFlow ~> output
      ClosedShape
    })
  }
}

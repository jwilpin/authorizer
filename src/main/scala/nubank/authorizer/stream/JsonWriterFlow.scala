package nubank.authorizer.stream

import akka.NotUsed
import akka.stream.scaladsl.Flow
import nubank.authorizer.api.AuthorizerApi.AuthorizationResult
import nubank.authorizer.json.JsonSupport
import nubank.authorizer.model.Account
import nubank.authorizer.stream.DefaultJsonWriterFlow.AuthorizationResultView
import org.json4s.jackson.Serialization

trait JsonWriterFlow {
  def writeJsonFlow: Flow[AuthorizationResult, String, NotUsed]
}

trait DefaultJsonWriterFlow extends JsonWriterFlow with JsonSupport {
  override def writeJsonFlow: Flow[AuthorizationResult, String, NotUsed] = {
    Flow[AuthorizationResult]
      .map(AuthorizationResultView.toView)
      .map(resultToJson)
  }

  private def resultToJson(result: AuthorizationResultView): String = {
    Serialization.write(result)
  }
}

object DefaultJsonWriterFlow {
  case class AuthorizationResultView(account: Option[Account], violations: Seq[String])

  object AuthorizationResultView {

    def toView(result: AuthorizationResult): AuthorizationResultView = {
      AuthorizationResultView(account = result.account, violations = result.violations.map(_.error))
    }
  }
}

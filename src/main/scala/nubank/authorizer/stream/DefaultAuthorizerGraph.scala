package nubank.authorizer.stream

trait DefaultAuthorizerGraph
  extends AuthorizerGraph
  with ConsoleSource
  with DefaultJsonReaderFlow
  with ConsoleOutputSink
  with DefaultAuthorizerFlow
  with DefaultJsonWriterFlow

object DefaultAuthorizerGraph extends DefaultAuthorizerGraph

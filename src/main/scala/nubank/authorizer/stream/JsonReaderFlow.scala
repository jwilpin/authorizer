package nubank.authorizer.stream

import akka.NotUsed
import akka.stream.scaladsl.Flow
import nubank.authorizer.action.AuthorizerAction
import nubank.authorizer.json.JsonSupport
import org.json4s.jackson.Serialization
import org.slf4j.LoggerFactory

import scala.util.{Failure, Success, Try}

trait JsonReaderFlow {
  def readJsonFlow: Flow[String, AuthorizerAction, NotUsed]
}

trait DefaultJsonReaderFlow extends JsonReaderFlow with JsonSupport {

  val logger = LoggerFactory.getLogger(getClass())

  override def readJsonFlow: Flow[String, AuthorizerAction, NotUsed] = {
    Flow[String]
      .map(jsonToAction)
      .filter(_.isDefined)
      .map(_.get)
  }

  private def jsonToAction(json: String): Option[AuthorizerAction] = {
    Try(Serialization.read[AuthorizerAction](json)) match {
      case Success(action) =>
        Option(action)
      case Failure(error) =>
        logger.warn(
          s"could not parse action: $json. Error: ${error.getMessage}"
        )
        None
    }
  }
}

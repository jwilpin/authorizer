package nubank.authorizer.stream

import akka.stream.IOResult
import akka.stream.scaladsl.{Source, StreamConverters}

import scala.concurrent.Future

trait InputSource {
  def input: Source[String, AnyRef]
}

trait ConsoleSource extends InputSource {
  override def input: Source[String, Future[IOResult]] = {
    StreamConverters
      .fromInputStream(() => System.in)
      .map(_.utf8String)
  }
}

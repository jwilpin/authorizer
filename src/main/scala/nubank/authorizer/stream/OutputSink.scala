package nubank.authorizer.stream

import akka.Done
import akka.stream.scaladsl.Sink

import scala.concurrent.Future

trait OutputSink {
  def output: Sink[String, Future[Done]]
}

trait ConsoleOutputSink extends OutputSink {
  override def output: Sink[String, Future[Done]] = {
    Sink.foreach(println)
  }
}

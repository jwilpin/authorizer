package nubank.authorizer.stream

import akka.NotUsed
import akka.stream.scaladsl.{Flow, Source}
import nubank.authorizer.action.AuthorizerAction
import nubank.authorizer.api.AuthorizerApi
import nubank.authorizer.api.AuthorizerApi.AuthorizationResult

trait AuthorizerFlow {
  def authorizeFlow(implicit authorizerApi: AuthorizerApi): Flow[AuthorizerAction, AuthorizationResult, NotUsed]
}

trait DefaultAuthorizerFlow extends AuthorizerFlow {
  override def authorizeFlow(
      implicit authorizerApi: AuthorizerApi
  ): Flow[AuthorizerAction, AuthorizationResult, NotUsed] = {
    Flow[AuthorizerAction]
      .flatMapConcat { action =>
        Source.future(authorizerApi.authorize(action))
      }
  }
}

package nubank.authorizer.json

import nubank.authorizer.json.AuthorizerActionParser.AuthorizerActionSerializer
import nubank.authorizer.json.DateTimeParser.DateTimeSerializer
import org.json4s._

trait JsonSupport {
  implicit val formats = DefaultFormats + DateTimeSerializer + AuthorizerActionSerializer
}

package nubank.authorizer.json

import org.joda.time.format.{DateTimeFormat, DateTimeFormatterBuilder}
import org.joda.time.{DateTime, DateTimeZone}
import org.json4s.CustomSerializer
import org.json4s.JsonAST.{JNull, JString}

object DateTimeParser {

  private val fmt = {
    val fmt = DateTimeFormat
      .forPattern("YYYY-MM-dd'T'HH:mm:ss.SSS'Z'")
      .withZone(DateTimeZone.UTC)
      .getParser
    new DateTimeFormatterBuilder().append(null, Array(fmt)).toFormatter
  }

  case object DateTimeSerializer
      extends CustomSerializer[DateTime](
        format =>
          ({
            case JString(s) => fmt.parseDateTime(s)
            case JNull      => null
          }, {
            case d: DateTime => JString(format.dateFormat.format(d.toDate))
          })
      )
}

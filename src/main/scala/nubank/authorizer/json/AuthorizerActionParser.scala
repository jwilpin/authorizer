package nubank.authorizer.json

import nubank.authorizer.action.{
  AuthorizeTransaction,
  AuthorizerAction,
  CreateAccount
}
import nubank.authorizer.json.DateTimeParser.DateTimeSerializer
import nubank.authorizer.model.{Account, Transaction}
import org.json4s.JsonAST.{JField, JObject}
import org.json4s.native.JsonMethods._
import org.json4s.native.Serialization.write
import org.json4s.{CustomSerializer, DefaultFormats, _}

object AuthorizerActionParser {

  implicit val formats = DefaultFormats + DateTimeSerializer

  case object AuthorizerActionSerializer
      extends CustomSerializer[AuthorizerAction](
        format =>
          ({
            case authorizerAction: JObject =>
              authorizerAction match {
                case JObject(
                    List(
                      JField(
                        "account",
                        JObject(
                          List(
                            JField("active-card", JBool(activeCard)),
                            JField("available-limit", JInt(availableLimit))
                          )
                        )
                      )
                    )
                    ) =>
                  CreateAccount(Account(activeCard, availableLimit.longValue()))
                case JObject(List(JField("transaction", transaction))) =>
                  AuthorizeTransaction(transaction.extract[Transaction])
              }
          }, {
            case action: AuthorizerAction => parse(write(action))
          })
      )
}

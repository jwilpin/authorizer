package nubank.authorizer.action

import nubank.authorizer.model.{Account, Transaction}

sealed trait AuthorizerAction

case class CreateAccount(account: Account) extends AuthorizerAction

case class AuthorizeTransaction(transaction: Transaction)
    extends AuthorizerAction

package nubank.authorizer

import akka.actor.ActorSystem
import akka.actor.typed.scaladsl.adapter._
import akka.util.Timeout
import nubank.authorizer.api.{AuthorizerActor, AuthorizerApi, EventDrivenAuthorizerApi}
import nubank.authorizer.stream.{AuthorizerGraph, DefaultAuthorizerGraph}
import nubank.authorizer.validator.{DefaultTransactionValidator, TransactionValidator}
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

object Main extends App {

  val logger = LoggerFactory.getLogger(getClass())
  logger.info("starting Authorizer app...")

  implicit val system = ActorSystem("AuthorizerActorSystem")
  implicit val ec: ExecutionContext = system.dispatcher
  implicit val scheduler = system.toTyped.scheduler
  implicit val requestTimeout: Timeout = 3 seconds

  val transactionValidator: TransactionValidator = DefaultTransactionValidator
  val authorizerActor = system.spawn(AuthorizerActor.init(transactionValidator), "AuthorizerActor")
  implicit val authorizerApi: AuthorizerApi = new EventDrivenAuthorizerApi(authorizerActor)

  val authorizerGraph: AuthorizerGraph = DefaultAuthorizerGraph

  logger.info("Authorizer app started...")

  authorizerGraph.graph.run()
}

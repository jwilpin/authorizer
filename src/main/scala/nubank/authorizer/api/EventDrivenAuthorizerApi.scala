package nubank.authorizer.api

import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.{ActorRef, Scheduler}
import akka.util.Timeout
import nubank.authorizer.action.AuthorizerAction
import nubank.authorizer.api.AuthorizerActor.AuthorizationMessage
import nubank.authorizer.api.AuthorizerApi.AuthorizationResult

import scala.concurrent.Future

class EventDrivenAuthorizerApi(authorizerActor: ActorRef[AuthorizationMessage])(
    implicit timeout: Timeout,
    scheduler: Scheduler
) extends AuthorizerApi {
  override def authorize(
      action: AuthorizerAction
  ): Future[AuthorizationResult] = {
    authorizerActor.?[AuthorizationResult](replyTo => AuthorizationMessage(action, replyTo))
  }
}

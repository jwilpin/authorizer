package nubank.authorizer.api

import nubank.authorizer.action.AuthorizerAction
import nubank.authorizer.api.AuthorizerApi.AuthorizationResult
import nubank.authorizer.model.Account
import nubank.authorizer.validator.Violation

import scala.concurrent.Future

trait AuthorizerApi {
  def authorize(action: AuthorizerAction): Future[AuthorizationResult]
}

object AuthorizerApi {
  case class AuthorizationResult(account: Option[Account], violations: Seq[Violation])
}

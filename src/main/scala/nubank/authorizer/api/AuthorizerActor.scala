package nubank.authorizer.api

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import nubank.authorizer.action.{AuthorizeTransaction, AuthorizerAction, CreateAccount}
import nubank.authorizer.api.AuthorizerApi.AuthorizationResult
import nubank.authorizer.model.{Account, Transaction}
import nubank.authorizer.validator.TransactionValidator
import nubank.authorizer.validator.Violation.AccountAlreadyInitializedViolation

object AuthorizerActor {
  case class AuthorizationMessage(action: AuthorizerAction, replyTo: ActorRef[AuthorizationResult])

  def init(transactionValidator: TransactionValidator): Behavior[AuthorizationMessage] = {
    creatingAccount(transactionValidator)
  }

  private def creatingAccount(
      implicit transactionValidator: TransactionValidator
  ): Behavior[AuthorizationMessage] = {
    Behaviors.receiveMessage[AuthorizationMessage] {
      case AuthorizationMessage(CreateAccount(account: Account), replyTo) =>
        replyTo ! AuthorizationResult(Option(account), Seq.empty)
        authorizingTransactions(account, Seq.empty)
      case AuthorizationMessage(AuthorizeTransaction(transaction: Transaction), replyTo) =>
        val violations = transactionValidator.validate(Option.empty, transaction, Seq.empty)
        replyTo ! AuthorizationResult(Option.empty, violations)
        Behaviors.same
    }
  }

  private def authorizingTransactions(account: Account, previous: Seq[Transaction])(
      implicit transactionValidator: TransactionValidator
  ): Behavior[AuthorizationMessage] = {
    Behaviors.receiveMessage[AuthorizationMessage] {
      case AuthorizationMessage(CreateAccount(_), replyTo) =>
        replyTo ! AuthorizationResult(Option(account), Seq(AccountAlreadyInitializedViolation))
        Behaviors.same
      case AuthorizationMessage(AuthorizeTransaction(transaction: Transaction), replyTo) =>
        val violations = transactionValidator.validate(Option(account), transaction, previous)
        if (violations.isEmpty) {
          val updatedAccount = account.copy(availableLimit = account.availableLimit - transaction.amount)
          replyTo ! AuthorizationResult(Option(updatedAccount), violations)
          authorizingTransactions(updatedAccount, previous :+ transaction)
        } else {
          replyTo ! AuthorizationResult(Option(account), violations)
          Behaviors.same
        }
    }
  }
}

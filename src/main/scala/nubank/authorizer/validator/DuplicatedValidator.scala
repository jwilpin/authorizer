package nubank.authorizer.validator

import nubank.authorizer.model.{Account, Transaction}
import nubank.authorizer.validator.DuplicatedValidator.Period
import nubank.authorizer.validator.Violation.DoubledTransactionViolation
import org.joda.time.{Interval, Minutes}

trait DuplicatedValidator extends TransactionValidatorBase {
  override def validate(account: Option[Account], transaction: Transaction, previous: Seq[Transaction]): Seq[Violation] = {
    val violations = super.validate(account, transaction, previous)
    val start = transaction.time.minus(Period)
    val end = transaction.time.plus(Period).plusMillis(1) // to be inclusive
    val interval = new Interval(start, end)
    previous
      .filter(tx => interval.contains(tx.time))
      .filter { tx =>
        tx.amount == transaction.amount && tx.merchant == transaction.merchant
      }
      .headOption
      .map(_ => violations :+ DoubledTransactionViolation)
      .getOrElse(violations)
  }
}

object DuplicatedValidator {
  val Period = Minutes.ONE
}

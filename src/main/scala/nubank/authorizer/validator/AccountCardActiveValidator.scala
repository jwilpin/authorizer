package nubank.authorizer.validator

import nubank.authorizer.model.{Account, Transaction}
import nubank.authorizer.validator.Violation.CardNotActiveViolation

trait AccountCardActiveValidator extends TransactionValidatorBase {
  override def validate(account: Option[Account], transaction: Transaction, previous: Seq[Transaction]): Seq[Violation] = {
    val violations = super.validate(account, transaction, previous)
    account
      .filterNot(_.activeCard)
      .map(_ => violations :+ CardNotActiveViolation)
      .getOrElse(violations)
  }
}

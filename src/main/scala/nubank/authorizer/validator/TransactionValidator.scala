package nubank.authorizer.validator

import nubank.authorizer.model.{Account, Transaction}

trait TransactionValidator {

  def validate(account: Option[Account],
               transaction: Transaction,
               previous: Seq[Transaction]): Seq[Violation]
}

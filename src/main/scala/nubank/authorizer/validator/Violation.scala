package nubank.authorizer.validator

case class Violation(error: String)

object Violation {
  val AccountAlreadyInitializedViolation = Violation("account-already-initialized")
  val CardNotActiveViolation = Violation("card-not-active")
  val AccountNotInitializedViolation = Violation("account-not-initialized")
  val InsufficientLimitViolation = Violation("insufficient-limit")
  val DoubledTransactionViolation = Violation("doubled-transaction")
  val HighFrequencySmallIntervalViolation = Violation("high-frequency-small-interval")
}

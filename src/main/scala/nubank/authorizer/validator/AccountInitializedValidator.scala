package nubank.authorizer.validator

import nubank.authorizer.model.{Account, Transaction}
import nubank.authorizer.validator.Violation.AccountNotInitializedViolation

trait AccountInitializedValidator extends TransactionValidatorBase {
  override def validate(account: Option[Account], transaction: Transaction, previous: Seq[Transaction]): Seq[Violation] = {
    val violations = super.validate(account, transaction, previous)
    account.map(_ => violations).getOrElse(violations :+ AccountNotInitializedViolation)
  }
}

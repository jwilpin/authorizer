package nubank.authorizer.validator

import nubank.authorizer.model.{Account, Transaction}
import nubank.authorizer.validator.FrequencyValidator.{MaxTransactionsPerInterval, Period}
import nubank.authorizer.validator.Violation.HighFrequencySmallIntervalViolation
import org.joda.time.{Interval, Minutes}

trait FrequencyValidator extends TransactionValidatorBase {
  override def validate(account: Option[Account], transaction: Transaction, previous: Seq[Transaction]): Seq[Violation] = {
    val violations = super.validate(account, transaction, previous)
    val start = transaction.time.minus(Period)
    val end = transaction.time.plus(Period).plusMillis(1) // to be inclusive
    val interval = new Interval(start, end)
    val transactions = previous.filter(tx => interval.contains(tx.time))
    if (transactions.size < MaxTransactionsPerInterval) {
      violations
    } else {
      violations :+ HighFrequencySmallIntervalViolation
    }
  }
}

object FrequencyValidator {
  val MaxTransactionsPerInterval = 3
  val Period = Minutes.ONE
}

package nubank.authorizer.validator

import nubank.authorizer.model.{Account, Transaction}
import nubank.authorizer.validator.Violation.InsufficientLimitViolation

trait AccountLimitValidator extends TransactionValidatorBase {
  override def validate(account: Option[Account], transaction: Transaction, previous: Seq[Transaction]): Seq[Violation] = {
    val violations = super.validate(account, transaction, previous)
    account
      .filter(_.availableLimit - transaction.amount < 0)
      .map(_ => violations :+ InsufficientLimitViolation)
      .getOrElse(violations)
  }
}

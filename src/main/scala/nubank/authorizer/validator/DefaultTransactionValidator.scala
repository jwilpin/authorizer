package nubank.authorizer.validator

trait DefaultTransactionValidator
    extends TransactionValidator
    with TransactionValidatorBase
    with AccountInitializedValidator
    with AccountCardActiveValidator
    with AccountLimitValidator
    with FrequencyValidator
    with DuplicatedValidator

object DefaultTransactionValidator extends DefaultTransactionValidator

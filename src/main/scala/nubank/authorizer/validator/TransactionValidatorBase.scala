package nubank.authorizer.validator

import nubank.authorizer.model.{Account, Transaction}

trait TransactionValidatorBase extends TransactionValidator {
  override def validate(account: Option[Account],
                        transaction: Transaction,
                        previous: Seq[Transaction]): Seq[Violation] = Seq.empty
}

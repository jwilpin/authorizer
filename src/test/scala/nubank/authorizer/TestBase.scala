package nubank.authorizer

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.{Inside, Inspectors, OptionValues, TestSuite}

trait TestBase extends TestSuite with Matchers with OptionValues with Inside with Inspectors

trait FlatTestBase extends AnyFlatSpec with TestBase

trait WordTestBase extends AnyWordSpec with TestBase

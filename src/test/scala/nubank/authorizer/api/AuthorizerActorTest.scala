package nubank.authorizer.api

import akka.actor.testkit.typed.scaladsl.{BehaviorTestKit, TestInbox}
import nubank.authorizer.WordTestBase
import nubank.authorizer.action.{AuthorizeTransaction, CreateAccount}
import nubank.authorizer.api.AuthorizerActor.AuthorizationMessage
import nubank.authorizer.api.AuthorizerApi.AuthorizationResult
import nubank.authorizer.model.{Account, Transaction}
import nubank.authorizer.validator.FrequencyValidator.MaxTransactionsPerInterval
import nubank.authorizer.validator.{DefaultTransactionValidator, TransactionValidator, Violation}
import org.joda.time.DateTime

class AuthorizerActorTest extends WordTestBase {

  val transactionValidator: TransactionValidator = DefaultTransactionValidator

  "AuthorizerActor" when {

    "CreateAccount action is received" should {

      "create an account successfully" in {
        val testKit = BehaviorTestKit(AuthorizerActor.init(transactionValidator))
        val inbox = TestInbox[AuthorizationResult]()

        val account = Account(true, 100)
        val action = CreateAccount(account)
        testKit.run(AuthorizationMessage(action, inbox.ref))
        inbox.expectMessage(AuthorizationResult(Option(account), Seq.empty))
      }

      "retrieve a violation for already created accounts" in {
        val testKit = BehaviorTestKit(AuthorizerActor.init(transactionValidator))
        val inbox = TestInbox[AuthorizationResult]()

        val account = Account(true, 100)
        val action = CreateAccount(account)

        testKit.run(AuthorizationMessage(action, inbox.ref))
        inbox.expectMessage(AuthorizationResult(Option(account), Seq.empty))

        testKit.run(AuthorizationMessage(action, inbox.ref))
        inbox.expectMessage(AuthorizationResult(Option(account), Seq(Violation.AccountAlreadyInitializedViolation)))
      }
    }

    "AuthorizeTransaction action is received" should {

      "apply a transaction successfully" in {
        val testKit = BehaviorTestKit(AuthorizerActor.init(transactionValidator))
        val inbox = TestInbox[AuthorizationResult]()

        val account = Account(true, 100)
        testKit.run(AuthorizationMessage(CreateAccount(account), inbox.ref))
        inbox.expectMessage(AuthorizationResult(Option(account), Seq.empty))

        val transaction = Transaction("merchant", 20, DateTime.now)
        val expectedAccount = account.copy(availableLimit = account.availableLimit - transaction.amount)
        testKit.run(AuthorizationMessage(AuthorizeTransaction(transaction), inbox.ref))
        inbox.expectMessage(AuthorizationResult(Option(expectedAccount), Seq.empty))
      }

      "retrieve a violation for non-initialized account" in {
        val testKit = BehaviorTestKit(AuthorizerActor.init(transactionValidator))
        val inbox = TestInbox[AuthorizationResult]()

        val transaction = Transaction("merchant", 20, DateTime.now)
        testKit.run(AuthorizationMessage(AuthorizeTransaction(transaction), inbox.ref))
        inbox.expectMessage(AuthorizationResult(Option.empty, Seq(Violation.AccountNotInitializedViolation)))
      }

      "retrieve a violation for inactive account card" in {
        val testKit = BehaviorTestKit(AuthorizerActor.init(transactionValidator))
        val inbox = TestInbox[AuthorizationResult]()

        val account = Account(false, 100)
        testKit.run(AuthorizationMessage(CreateAccount(account), inbox.ref))
        inbox.expectMessage(AuthorizationResult(Option(account), Seq.empty))

        val transaction = Transaction("merchant", 20, DateTime.now)
        testKit.run(AuthorizationMessage(AuthorizeTransaction(transaction), inbox.ref))
        inbox.expectMessage(AuthorizationResult(Option(account), Seq(Violation.CardNotActiveViolation)))
      }

      "retrieve a violation when overpass account limit" in {
        val testKit = BehaviorTestKit(AuthorizerActor.init(transactionValidator))
        val inbox = TestInbox[AuthorizationResult]()

        val account = Account(true, 100)
        testKit.run(AuthorizationMessage(CreateAccount(account), inbox.ref))
        inbox.expectMessage(AuthorizationResult(Option(account), Seq.empty))

        val transaction = Transaction("merchant", 120, DateTime.now)
        testKit.run(AuthorizationMessage(AuthorizeTransaction(transaction), inbox.ref))
        inbox.expectMessage(AuthorizationResult(Option(account), Seq(Violation.InsufficientLimitViolation)))
      }

      "retrieve a violation for duplicated transactions" in {
        val testKit = BehaviorTestKit(AuthorizerActor.init(transactionValidator))
        val inbox = TestInbox[AuthorizationResult]()

        val account = Account(true, 100)
        testKit.run(AuthorizationMessage(CreateAccount(account), inbox.ref))
        inbox.expectMessage(AuthorizationResult(Option(account), Seq.empty))

        val transaction = Transaction("merchant", 20, DateTime.now)
        val expectedAccount = account.copy(availableLimit = account.availableLimit - transaction.amount)
        testKit.run(AuthorizationMessage(AuthorizeTransaction(transaction), inbox.ref))
        inbox.expectMessage(AuthorizationResult(Option(expectedAccount), Seq.empty))

        testKit.run(AuthorizationMessage(AuthorizeTransaction(transaction), inbox.ref))
        inbox.expectMessage(AuthorizationResult(Option(expectedAccount), Seq(Violation.DoubledTransactionViolation)))
      }

      "retrieve a violation when overpass max allowed transactions per interval" in {
        val testKit = BehaviorTestKit(AuthorizerActor.init(transactionValidator))
        val inbox = TestInbox[AuthorizationResult]()

        val account = Account(true, 100)
        testKit.run(AuthorizationMessage(CreateAccount(account), inbox.ref))
        inbox.expectMessage(AuthorizationResult(Option(account), Seq.empty))

        (1 to MaxTransactionsPerInterval).foreach { index =>
          val transaction = Transaction(s"merchant $index", 20, DateTime.now.plusSeconds(index))
          val expectedAccount = account.copy(availableLimit = account.availableLimit - transaction.amount * index)
          testKit.run(AuthorizationMessage(AuthorizeTransaction(transaction), inbox.ref))
          inbox.expectMessage(AuthorizationResult(Option(expectedAccount), Seq.empty))
        }

        val transaction = Transaction("merchant", 20, DateTime.now)
        val expectedAccount =
          account.copy(availableLimit = account.availableLimit - transaction.amount * MaxTransactionsPerInterval)
        testKit.run(AuthorizationMessage(AuthorizeTransaction(transaction), inbox.ref))
        inbox.expectMessage(
          AuthorizationResult(Option(expectedAccount), Seq(Violation.HighFrequencySmallIntervalViolation))
        )
      }
    }
  }
}

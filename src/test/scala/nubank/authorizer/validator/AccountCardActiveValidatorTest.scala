package nubank.authorizer.validator

import nubank.authorizer.FlatTestBase
import nubank.authorizer.model.{Account, Transaction}
import nubank.authorizer.validator.Violation.CardNotActiveViolation
import org.joda.time.DateTime

class AccountCardActiveValidatorTest extends FlatTestBase with AccountCardActiveValidator {

  private val transaction = Transaction("merchant", 100, DateTime.now)

  "AccountCardActiveValidator" should "validate transactions with inactive account card" in {
    val account = Account(false, 100)
    val violations = validate(Option(account), transaction, Seq.empty)
    violations shouldBe Seq(CardNotActiveViolation)
  }

  it should "return empty violation for transactions with active account card" in {
    val account = Account(true, 100)
    val violations = validate(Option(account), transaction, Seq.empty)
    violations shouldBe empty
  }

  it should "return empty violation for transactions without initialized account" in {
    val account = Account(true, 100)
    val violations = validate(Option.empty, transaction, Seq.empty)
    violations shouldBe empty
  }
}

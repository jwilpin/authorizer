package nubank.authorizer.validator

import nubank.authorizer.FlatTestBase
import nubank.authorizer.model.{Account, Transaction}
import nubank.authorizer.validator.DuplicatedValidator.Period
import nubank.authorizer.validator.Violation.DoubledTransactionViolation
import org.joda.time.DateTime

class DuplicatedValidatorTest extends FlatTestBase with DuplicatedValidator {

  private val account = Account(true, 100)

  "DuplicatedValidator" should "validate duplicated transaction when previous ones have the same time" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val previous = transaction
    val violations = validate(Option(account), transaction, Seq(previous))
    violations shouldBe Seq(DoubledTransactionViolation)
  }

  it should "validate duplicated transaction when previous ones is in the interval (after)" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val previous = transaction.copy(time = transaction.time.plusSeconds(1))
    val violations = validate(Option(account), transaction, Seq(previous))
    violations shouldBe Seq(DoubledTransactionViolation)
  }

  it should "validate duplicated transaction when previous ones is in the interval (before)" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val previous = transaction.copy(time = transaction.time.minusSeconds(1))
    val violations = validate(Option(account), transaction, Seq(previous))
    violations shouldBe Seq(DoubledTransactionViolation)
  }

  it should "validate duplicated transaction when previous ones is in the interval (start limit)" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val previous = transaction.copy(time = transaction.time.minus(Period))
    val violations = validate(Option(account), transaction, Seq(previous))
    violations shouldBe Seq(DoubledTransactionViolation)
  }

  it should "validate duplicated transaction when previous ones is in the interval (end limit)" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val previous = transaction.copy(time = transaction.time.plus(Period))
    val violations = validate(Option(account), transaction, Seq(previous))
    violations shouldBe Seq(DoubledTransactionViolation)
  }

  it should "return empty violation for transactions out of range" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val previous = Seq(
      transaction.copy(time = transaction.time.minus(Period).minusMillis(1)),
      transaction.copy(time = transaction.time.plus(Period).plusMillis(1))
    )
    val violations = validate(Option(account), transaction, previous)
    violations shouldBe empty
  }

  it should "return empty violation for transactions with different amount or merchant" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val previous = Seq(
      transaction.copy(merchant = "other merchant"),
      transaction.copy(amount = 200)
    )
    val violations = validate(Option(account), transaction, previous)
    violations shouldBe empty
  }
}

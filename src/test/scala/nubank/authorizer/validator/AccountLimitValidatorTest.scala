package nubank.authorizer.validator

import nubank.authorizer.FlatTestBase
import nubank.authorizer.model.{Account, Transaction}
import nubank.authorizer.validator.Violation.InsufficientLimitViolation
import org.joda.time.DateTime

class AccountLimitValidatorTest extends FlatTestBase with AccountLimitValidator {

  "AccountLimitValidator" should "validate transaction when overpass account limit" in {
    val account = Account(true, 100)
    val transaction = Transaction("merchant", 101, DateTime.now)
    val violations = validate(Option(account), transaction, Seq.empty)
    violations shouldBe Seq(InsufficientLimitViolation)
  }

  it should "return empty violation when transaction amount is lower than account limit" in {
    val account = Account(true, 100)
    val transaction = Transaction("merchant", 99, DateTime.now)
    val violations = validate(Option(account), transaction, Seq.empty)
    violations shouldBe empty
  }

  it should "return empty violation when transaction amount is equal than account limit" in {
    val account = Account(true, 100)
    val transaction = Transaction("merchant", 100, DateTime.now)
    val violations = validate(Option(account), transaction, Seq.empty)
    violations shouldBe empty
  }

  it should "return empty violation for non initialized accounts" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val violations = validate(Option.empty, transaction, Seq.empty)
    violations shouldBe empty
  }
}

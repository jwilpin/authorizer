package nubank.authorizer.validator

import nubank.authorizer.FlatTestBase
import nubank.authorizer.model.{Account, Transaction}
import nubank.authorizer.validator.FrequencyValidator.{MaxTransactionsPerInterval, Period}
import nubank.authorizer.validator.Violation.HighFrequencySmallIntervalViolation
import org.joda.time.DateTime

class FrequencyValidatorTest extends FlatTestBase with FrequencyValidator {

  private val account = Account(true, 100)

  "FrequencyValidator" should "validate max allowed transactions per interval when previous have the same time" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val previous = Seq.fill(MaxTransactionsPerInterval)(transaction)
    val violations = validate(Option(account), transaction, previous)
    violations shouldBe Seq(HighFrequencySmallIntervalViolation)
  }

  it should "validate max allowed transactions per interval when previous are in the interval (after)" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val previous = Seq.fill(MaxTransactionsPerInterval)(
      transaction.copy(time = transaction.time.plusSeconds(1))
    )
    val violations = validate(Option(account), transaction, previous)
    violations shouldBe Seq(HighFrequencySmallIntervalViolation)
  }

  it should "validate max allowed transactions per interval when previous are in the interval (before)" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val previous = Seq.fill(MaxTransactionsPerInterval)(
      transaction.copy(time = transaction.time.minusSeconds(1))
    )
    val violations = validate(Option(account), transaction, previous)
    violations shouldBe Seq(HighFrequencySmallIntervalViolation)
  }

  it should "validate max allowed transactions per interval when previous are in the interval (start limit)" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val previous = Seq.fill(MaxTransactionsPerInterval)(
      transaction.copy(time = transaction.time.minus(Period))
    )
    val violations = validate(Option(account), transaction, previous)
    violations shouldBe Seq(HighFrequencySmallIntervalViolation)
  }

  it should "validate max allowed transactions per interval when previous are in the interval (end limit)" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val previous = Seq.fill(MaxTransactionsPerInterval)(
      transaction.copy(time = transaction.time.plus(Period))
    )
    val violations = validate(Option(account), transaction, previous)
    violations shouldBe Seq(HighFrequencySmallIntervalViolation)
  }

  it should "return empty violation for transactions for empty previous transactions" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val previous = Seq.empty
    val violations = validate(Option(account), transaction, previous)
    violations shouldBe empty
  }

  it should "return empty violation for transactions for not enough previous duplicated transactions" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val previous = Seq.fill(MaxTransactionsPerInterval - 1)(transaction)
    val violations = validate(Option(account), transaction, previous)
    violations shouldBe empty
  }

  it should "return empty violation for transactions for previous out of range transactions" in {
    val transaction = Transaction("merchant", 100, DateTime.now)
    val previous =
      Seq.fill(MaxTransactionsPerInterval)(
        transaction.copy(time = transaction.time.minus(Period).minusMillis(1))
      ) ++ Seq.fill(MaxTransactionsPerInterval)(
        transaction.copy(time = transaction.time.plus(Period).plusMillis(1))
      )
    val violations = validate(Option(account), transaction, previous)
    violations shouldBe empty
  }
}

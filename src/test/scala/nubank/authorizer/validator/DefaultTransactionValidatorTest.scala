package nubank.authorizer.validator

import nubank.authorizer.FlatTestBase
import nubank.authorizer.model.{Account, Transaction}
import nubank.authorizer.validator.FrequencyValidator.MaxTransactionsPerInterval
import nubank.authorizer.validator.Violation._
import org.joda.time.DateTime

class DefaultTransactionValidatorTest extends FlatTestBase with DefaultTransactionValidator {

  "DefaultTransactionValidator" should "stack violations when initialized account" in {
    // account card inactive issue
    val account = Account(false, 100)
    // account limit issue
    val transaction = Transaction("merchant", 1000, DateTime.now)
    // duplicate and frequency issue
    val previous = Seq.fill(MaxTransactionsPerInterval)(transaction)
    val violations = validate(Option(account), transaction, previous)
    violations should contain theSameElementsAs Seq(
      CardNotActiveViolation,
      InsufficientLimitViolation,
      DoubledTransactionViolation,
      HighFrequencySmallIntervalViolation
    )
  }

  it should "stack violations when non-initialized account" in {
    // non-initialized account issue
    val transaction = Transaction("merchant", 1000, DateTime.now)
    // duplicate and frequency issue
    val previous = Seq.fill(MaxTransactionsPerInterval)(transaction)
    val violations = validate(Option.empty, transaction, previous)
    violations should contain theSameElementsAs Seq(
      AccountNotInitializedViolation,
      DoubledTransactionViolation,
      HighFrequencySmallIntervalViolation
    )
  }
}

package nubank.authorizer.validator

import nubank.authorizer.FlatTestBase
import nubank.authorizer.model.{Account, Transaction}
import nubank.authorizer.validator.Violation.AccountNotInitializedViolation
import org.joda.time.DateTime

class AccountInitializedValidatorTest extends FlatTestBase with AccountInitializedValidator {

  private val transaction = Transaction("merchant", 100, DateTime.now)

  "AccountInitializedValidator" should "validate transactions without initialized account" in {
    val violations = validate(Option.empty, transaction, Seq.empty)
    violations shouldBe Seq(AccountNotInitializedViolation)
  }

  it should "return empty violation for transactions with initialized account" in {
    val account = Account(true, 100)
    val violations = validate(Option(account), transaction, Seq.empty)
    violations shouldBe empty
  }
}
